const path = require('path');
require('dotenv').config();

const host = process.env.HOST || 'localhost';
const port = process.env.PORT || 3000;
const apiUrl = process.env.APIURL || `http://${host}:${port}/api`

module.exports = {
  head: {
    title: 'Kingdom Death: Monster - Resource Manager',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ]
  },
  env: {
    baseUrl: `http://${host}:${port}`,
    apiUrl
  },
  dev: process.env.NODE_ENV !== 'production',
  srcDir: 'client/',
  rootDir: path.resolve(__dirname),
  link: [
    { rel: 'icon', type: 'ximage/x-icon', href: '/favicon.ico', }
  ],
  loading: { color: '#a30303' },
  build: {
    vendor: ['axios']
  }
}
