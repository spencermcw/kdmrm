
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const settlements = new Schema({
    name: {
      type: String,
      required: [true, 'A settlement must have a name.']
    },
    population: {
      type: Number,
      default: 1,
      min: [0, 'Min: 0'],
      required: true
    },
    timeline: [{
      _id: false,
      complete: { type: Boolean, default: false },
      nemesis: { type: Boolean },
      story_event: String,
      settlement_event: String
    }],
    deaths: {
      type: Number,
      default: 0,
      min: [0, 'Min: 0'],
      required: true
    },
    survival: {
      limit: {
        type: Number,
        default: 1,
        min: [0, 'Min: 0'],
        required: true
      },
      departing: {
        type: Number,
        default: 1,
        min: [0, 'Min: 0'],
        required: true
      }
    },
    milestones: [{
      _id: false,
      trigger: String,
      triggered: { type: Boolean, default: false },
      story_event: String
    }],
    principles: [{
      _id: false,
      name: String,
      triggered: { type: Boolean, default: false },
      choice: { type: Boolean, default: false },
      choices: [String]
    }],
    lantern_research: {
      type: Number,
      min: [0, 'Min: 0'],
      required: true,
      default: 0
    },
    innovations: {
      type: Array,
      default: ['Language']
    },
    settlement_locations: {
      type: Array,
      default: ['Lantern Hoard']
    },
    quarries: {
      type: Array,
      default: ['White Lion']
    },
    gear: [{
      _id: false,
      card: { type: Object, required: true },
      total: { type: Number, required: true, default: 1 },
    }],
    resources: [{
      _id: false,
      card: { type: Object, required: true },
      total: { type: Number, required: true, default: 1 },
    }],
    monster_volumes: [String],
    notes: String,
    newborn_template: {
      type: Schema.Types.ObjectId,
      ref: 'Survivor'
    },
  },
  {
    timestamps: true
  });

  return mongooseClient.model('settlements', settlements);
};
