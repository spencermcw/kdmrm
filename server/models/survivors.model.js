
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const survivors = new Schema({
    name: { type: String, required: [true, 'A survivor must surely have a name.'] },
    male: { type: Boolean },
    dead: { type: Boolean, default: false },
    settlement: { type: Schema.Types.ObjectId, ref: 'Settlement' },
    xp: { type: Number, default: 0, min: [0, 'Min: 0'], max: [16, 'Max: 16'] },
    survival: {
      value: { type: Number, default: 1, min: [0, 'Min: 0'] },
      dodge: { type: Boolean, default: false },
      encourage: { type: Boolean, default: false },
      surge: { type: Boolean, default: false },
      dash: { type: Boolean, default: false },
      endure: { type: Boolean, default: false },
    },
    skip_hunt: { type: Boolean, default: false },
    attributes: {
      movement: {
        base: { type: Number, default: 5 },
        token: { type: Number, default: 0 },
      },
      accuracy: { 
        base: { type: Number, default: 0 },
        token: { type: Number, default: 0 },
      },
      strength: {
        base: { type: Number, default: 0 },
        token: { type: Number, default: 0 },
      },
      evasion: { 
        base: { type: Number, default: 0 },
        token: { type: Number, default: 0 },
      },
      luck: { 
        base: { type: Number, default: 0 },
        token: { type: Number, default: 0 },
      },
      speed: {
        base: { type: Number, default: 0 },
        token: { type: Number, default: 0 },
      },
    },
    stats: {
      bleed: { type: Number, default: 0 },
      brain: { type: Number, default: 0 },
      lunacy: { type: Number, default: 0 },
      head: {
        armor: { type: Number, default: 0 },
        injured_heavy: { type: Boolean, default: false }
      },
      arms: {
        armor: { type: Number, default: 0 },
        injured_light: { type: Boolean, default: false },
        injured_heavy: { type: Boolean, default: false }
      },
      body: {
        armor: { type: Number, default: 0 },
        injured_light: { type: Boolean, default: false },
        injured_heavy: { type: Boolean, default: false }
      },
      waist: {
        armor: { type: Number, default: 0 },
        injured_light: { type: Boolean, default: false },
        injured_heavy: { type: Boolean, default: false }
      },
      legs: {
        armor: { type: Number, default: 0 },
        injured_light: { type: Boolean, default: false },
        injured_heavy: { type: Boolean, default: false }
      },
    },
    courage: {
      value: { type: Number, default: 0, min: [0, 'Min: 0'], max: [9, 'Max: 9'] },
      stalwart: { type: Boolean, default: false },
      prepared: { type: Boolean, default: false },
      matchmaker: { type: Boolean, default: false }
    },
    understanding: {
      value: { type: Number, default: 0, min: [0, 'Min: 0'], max: [9, 'Max: 9'] },
      analyze: { type: Boolean, default: false },
      explore: { type: Boolean, default: false },
      tinker: { type: Boolean, default: false }
    },
    weapon_proficiency: {
      value: { type: Number, default: 0, min: [0, 'Min: 0'], max: [8, 'Max: 8']},
      kind: String,
      specialist: String,
      master: String,
    },
    severe_injuries: {
      intracranial_hemmorage: { type: Boolean, default: false },
      deaf: { type: Boolean, default: false },
      blind: { type: Number, default: 0, max: [2, 'Max: 2'], min: [0, 'Min: 0'] },
      shattered_jaw: { type: Boolean, default: false },

      dismembered_arm: { type: Number, default: 0, max: [2, 'Max: 2'], min: [0, 'Min: 0'] },
      ruptured_muscle: { type: Boolean, default: false },
      contracture: { type: Number, default: 0, min: [0, 'Min: 0'] },
      broken_arm: { type: Number, default: 0, max: [2, 'Max: 2'], min: [0, 'Min: 0'] },

      gaping_chest_wound: { type: Number, default: 0, min: [0, 'Min: 0'] },
      destroyed_back: { type: Boolean, default: false },
      broken_rib: { type: Number, default: 0, min: [0, 'Min: 0'] },

      intestinal_prolapse: { type: Boolean, default: false },
      warped_pelvis: { type: Number, default: 0, min: [0, 'Min: 0'] },
      destroyed_genitals: { type: Boolean, default: false },
      broken_hip: { type: Boolean, default: false },
      
      dismembered_leg: { type: Number, default: 0, max: [2, 'Max: 2'], min: [0, 'Min: 0'] },
      hamstrung: { type: Boolean, default: false },
      broken_leg: { type: Number, default: 0, max: [2, 'Max: 2'], min: [0, 'Min: 0'] }
    },
    gear: { type: Array, default: Array(9) },
    statuses: Array,
    notes: String,
    template: { type: Boolean, default: false },
  }, {
    timestamps: true
  });

  return mongooseClient.model('survivors', survivors);
};
