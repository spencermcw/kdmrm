// cards-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const cards = new Schema({
    title: { type: String, required: true },
    text: [String],
    kind: { type: String, required: true, enum: ['fighting_art', 'secret_fighting_art', 'disorder', 'gear', 'basic_resource', 'armor', 'weapon', 'item', 'terrain']},
    /* GEAR */
    armor: Number,
    armor_location: { type: String, enum: ['head', 'body', 'arms', 'waist', 'legs']},
    /* WEAPONS */
    stats: { type: [String], default: undefined },
    affinities: {
      type: [{
        _id: false,
        location: { type: String, enum: ['n','s','e','w'] },
        color: { type: String, enum: ['red', 'blue', 'green'] },
      }],
      default: undefined
    },
    bonuses: {
      type: [{
        _id: false,
        color: { type: String, enum: ['red', 'blue', 'green'] },
        kind: { type: String, enum: ['piece', 'square'] },
        rule: String
      }],
      default: undefined
    },
    /* ALL */
    keywords: { type: [String], default: undefined }
  });

  return mongooseClient.model('cards', cards);
};
