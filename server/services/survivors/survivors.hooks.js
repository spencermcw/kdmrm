
const capitalize = require('../../hooks/capitalize');

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [capitalize()],
    update: [capitalize()],
    patch: [capitalize()],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
