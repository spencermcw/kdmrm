const settlements = require('./settlements/settlements.service.js');
const survivors = require('./survivors/survivors.service.js');
const cards = require('./cards/cards.service.js');
module.exports = function (app) {
  app.configure(settlements);
  app.configure(survivors);
  app.configure(cards);
};
