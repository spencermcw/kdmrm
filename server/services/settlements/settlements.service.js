// Initializes the `settlements` service on path `/api/settlements`
const createService = require('feathers-mongoose');
const createModel = require('../../models/settlements.model');
const hooks = require('./settlements.hooks');

module.exports = function (app) {
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'settlements',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/api/settlements', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('api/settlements');

  service.hooks(hooks);
};
