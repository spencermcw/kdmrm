/* eslint-disable no-console */
require('dotenv').config();
const logger = require('winston');
const app = require('./app');
const host = process.env.HOST;
const port = process.env.PORT;

process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);

process.on('nuxt:build:done', (err) => {
  if (err) {
    logger.error(err);
  }

  if (!port) {
    return logger.error('ERROR: PORT: ' + port);
  }

  const server = app.listen(port);

  server.on('listening', () =>
    logger.info('Feathers application started on http://%s:%d', host, port)
  );
});
