// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
const _ = require('lodash');

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return context => {
    if (context.data.name)
      context.data.name = _.toUpper(context.data.name);
    return context;
  };
};
