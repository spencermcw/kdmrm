//const logger = require('winston');
const { render } = require('./nuxt');

const feathersRoutes = ['api'];

module.exports = function (app) { // eslint-disable-line no-unused-vars
  app.use((req, res, next) => {
    // FeathersJS Routing
    if (feathersRoutes.includes(req.path.split('/')[1])) {
      next();
    } else {
      // Nuxt Routing
      render(req, res, next);
    }
  });
};
