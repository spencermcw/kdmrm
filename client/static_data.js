export default {
  milestones:
  [
    { trigger: "First Birth", story_event: "Principle: New Life" },
    { trigger: "First Death", story_event: "Principle: Death" },
    { trigger: "5 Innovations", story_event: "Hooded Knight" },
    { trigger: "15 Population", story_event: "Principle: Society" },
    { trigger: "0 Population", story_event: "Game Over" },
  ],
  principles:
  [
    { name: "New Life", choices: [ "Protect the Young", "Survival of the Fittest" ] },
    { name: "Death", choices: [ "Cannibalize", "Graves" ] },
    { name: "Society", choices: [ "Collective Toil", "Accept Darkness" ] },
    { name: "Conviction", choices: [ "Barbaric", "Romantic" ] }
  ],
  timeline:
  [
    { story_event: "Returning Survivors", settlement_event: "First Story" },
    { story_event: "Endless Screams" },
    { story_event: "" },
    { story_event: "The Butcher Lvl 1", nemesis: true },
    { story_event: "Hands of Heat" }, // 5
    { story_event: "Armored Stranger" },
    { story_event: "Phoenix Feather" },
    { story_event: "" },
    { story_event: "Kings Man Lvl 1", nemesis: true },
    { story_event: "" }, // 10
    { story_event: "Regal Visit" },
    { story_event: "Principle: Conviction" },
    { story_event: "The Hand Lvl 1", nemesis: true },
    { story_event: "" },
    { story_event: "" }, // 15
    { story_event: "The Butcher Lvl 2", nemesis: true },
    { story_event: "" },
    { story_event: "" },
    { story_event: "Kings Man Lvl 2", nemesis: true },
    { story_event: "Watched" }, // 20
    { story_event: "" },
    { story_event: "" },
    { story_event: "Butcher Lvl 3", nemesis: true },
    { story_event: "" },
    { story_event: "Watcher", nemesis: true }, // 25
    { story_event: "" },
    { story_event: "" },
    { story_event: "Gold Smoke Knight", nemesis: true },
    { story_event: "" },
    { story_event: "" } // 30
  ]
}
