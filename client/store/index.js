import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    state: {
      cards: []
    },
    getters: {
      cards: state => {
        if (state.cards.length === 0) {
          axios.get(process.env.apiUrl + '/cards?$limit=500')
          .then(res => {
            state.cards = res.data.data;
          }).catch(err => console.log(err));
        }
        return state.cards;
      }
    }
  })
}

export default createStore
