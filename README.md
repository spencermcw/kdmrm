# Kingdom Death: Monster - Resource Manager

>

## About

This project is built on a [FeathersJS](http://feathersjs.com) back-end with a [Nuxt](http://nuxtjs.org) front-end.

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies
    ```
    cd path/to/kdmrm; npm install
    ```
3. Start your app

    ```
    npm start
    ```

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Changelog

__0.0.1__

- Initial release

## License

Copyright (c) 2017

Licensed under the [MIT license](LICENSE).
