const assert = require('assert');
const app = require('../../server/app');

describe('\'cards\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/cards');

    assert.ok(service, 'Registered the service');
  });
});
