const assert = require('assert');
const app = require('../../server/app');

describe('\'survivors\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/survivors');

    assert.ok(service, 'Registered the service');
  });
});
