const assert = require('assert');
const app = require('../../server/app');

describe('\'settlements\' service', () => {
  it('registered the service', () => {
    const service = app.service('api/settlements');

    assert.ok(service, 'Registered the service');
  });
});
