const assert = require('assert');
const axios = require('axios');
const logger = require('winston');
const url = require('url');
const app = require('../server/app');

const { Nuxt, Builder } = require('nuxt');
const config = require('../nuxt.config');
const nuxt = new Nuxt(config);

const port = app.get('port') || 3030;
const getUrl = pathname => url.format({
  hostname: app.get('host') || 'localhost',
  protocol: 'http',
  port,
  pathname
});

describe('Feathers application tests', () => {
  before(function(done) {
    new Builder(nuxt).build()
      .then(() => process.emit('nuxt:test:build:done'))
      .catch((error) => {
        logger.error(error);
        process.exit(1);
      });

    process.on('nuxt:test:build:done', err => {
      if(err) {
        logger.error(err);
      }
      this.server = app.listen(port);
      this.server.once('listening', () => done());
    });
  });

  after(function(done) {
    this.server.close(done);
  });

  it('starts and shows the index page', done => {
    axios(getUrl())
      .then(res => {
        assert.ok(res.status === 200);
        done();
      })
      .catch(err => {
        logger.error(err);
      });
  });
});
